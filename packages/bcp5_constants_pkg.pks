CREATE OR REPLACE PACKAGE bcp5_constants_pkg
/******************************************************************************
$Date:15.02.2021 19:42:33$
$Author:V.Kostevych$
$Id:bcp5_constants_pkg.pks$
$Revision:58$
$HeadURL:https://nexus.test.kv.aval/repository/BCPay-Deliveries/BCPay-DB/BCPay-DB-vx.x.x_20210215.zip$
$JIRA:BR-2$
******************************************************************************/

--444

IS

   /* *****************************************************************************
      NAME:       BCP5_CONSTANTS_PKG
      PURPOSE:    ����� ������ ���������

      REVISIONS:
      Ver        Date        Author           Jira              Description
      ---------  ----------  ---------------  ----------------  ------------------------------------
      4.2.0      20140508    OFrolyak         MIDASDEC-1765     1. Create this package.
      5.1.13     20151209    OFrolyak         SBCPAY-785        2. �������� ���������� STATUS_EVENT_ACC_FROM_PROC
      5.2.3      20160817    OFrolyak         DTASKS-4953       3. ������������� ���� USER_ROLE_47, USER_ROLE_52, USER_ROLE_48, USER_ROLE_49, USER_ROLE_51, user_role_88, user_role_88, USER_ROLE_79, user_role_78
                 20160822    OFrolyak         DTASKS-4953       4. ��������� role_group_teller, role_group_supervisor, role_group_report_manager, role_group_expert_its, role_group_manager_its
      5.3.3      20180420    OFrolyak         WMBT-2409         5. Added procedure doc_status_e, constants gc_batch_processing_yes, gc_batch_processing_no
      6.0.3      20191017    VKostevych       LCY-78,L�Y-117,120   Add functions user_name_bp_checker,doc_status_i
      7.1.0      20200902    VSilenko        LCY-431            7. ��������� ����������� ������ � UT-������ (ENV_PRODUCTION)
     ***************************************************************************** */

   gc_true                      CONSTANT VARCHAR2(4) := 'TRUE';
   gc_false                     CONSTANT VARCHAR2(5) := 'FALSE';

   gc_batch_processing_yes      CONSTANT VARCHAR2(1) := 'Y';
   gc_batch_processing_no       CONSTANT VARCHAR2(1) := 'N';

   ENV_PRODUCTION               CONSTANT BOOLEAN     := true; --DEV, REALISE - false, ABS - true

   FUNCTION y
      RETURN VARCHAR2;
   FUNCTION n
      RETURN VARCHAR2;

   gc_0                         CONSTANT VARCHAR2(1) := '0';
   gc_1                         CONSTANT VARCHAR2(1) := '1';

   FUNCTION doc_status_h
      RETURN VARCHAR2;
   FUNCTION doc_status_f
      RETURN VARCHAR2;
   FUNCTION doc_status_p
      RETURN VARCHAR2;
   FUNCTION doc_status_x
      RETURN VARCHAR2;
   FUNCTION doc_status_z
      RETURN VARCHAR2;
   FUNCTION doc_status_s
      RETURN VARCHAR2;
   FUNCTION doc_status_b
      RETURN VARCHAR2;
   FUNCTION doc_status_c
      RETURN VARCHAR2;
   FUNCTION doc_status_v
      RETURN VARCHAR2;
   FUNCTION doc_status_unknown
      RETURN VARCHAR2;
   FUNCTION doc_status_y
      RETURN VARCHAR2;
   FUNCTION doc_status_a
      RETURN VARCHAR2;
   FUNCTION doc_status_d
      RETURN VARCHAR2;
   FUNCTION doc_status_e
      RETURN VARCHAR2;
   FUNCTION doc_status_i
      RETURN VARCHAR2;


   FUNCTION status_event_rem_from_proc
      RETURN VARCHAR2;
   FUNCTION status_event_factura_role_47
      RETURN VARCHAR2;
   FUNCTION status_event_acc_to_proc
      RETURN VARCHAR2;
   FUNCTION status_event_acc_to_proc_refix
      RETURN VARCHAR2;
   FUNCTION status_event_acc_to_proc_supp
      RETURN VARCHAR2;
   FUNCTION STATUS_EVENT_ACC_FROM_PROC
      RETURN VARCHAR2;

   FUNCTION user_role_18
      RETURN VARCHAR2;
   FUNCTION user_role_xx
      RETURN VARCHAR2;
      /*
   FUNCTION user_role_47
      RETURN VARCHAR2;
   FUNCTION user_role_52
      RETURN VARCHAR2;
   FUNCTION user_role_79
      RETURN VARCHAR2;
   FUNCTION user_role_78
      RETURN VARCHAR2;
   FUNCTION user_role_48
      RETURN VARCHAR2;
   FUNCTION user_role_49
      RETURN VARCHAR2;
   FUNCTION user_role_51
      RETURN VARCHAR2;
   FUNCTION user_role_88
      RETURN VARCHAR2;
      */

   FUNCTION role_group_teller RETURN NUMBER;
   FUNCTION role_group_supervisor RETURN NUMBER;
   FUNCTION role_group_report_manager RETURN NUMBER;
   FUNCTION role_group_expert_its RETURN NUMBER;
   FUNCTION role_group_manager_its RETURN NUMBER;
   FUNCTION role_group_tech_bank_system RETURN NUMBER;

   FUNCTION system_id_bcpay_online
      RETURN NUMBER;
   FUNCTION system_id_bcpay_offline
      RETURN NUMBER;
   FUNCTION system_id_bcpay_stp
      RETURN NUMBER;
   FUNCTION system_id_mbroker
      RETURN NUMBER;

   FUNCTION system_mnemo_bcpay_online
      RETURN VARCHAR2;
   FUNCTION system_mnemo_bcpay_offline
      RETURN VARCHAR2;
   FUNCTION system_mnemo_bcpay_stp
      RETURN VARCHAR2;
   FUNCTION system_mnemo_mbroker
      RETURN VARCHAR2;

   FUNCTION user_name_bp_checker
      RETURN VARCHAR2;
   FUNCTION user_name_bp_mbroker
      RETURN VARCHAR2;
   FUNCTION user_role_bp_mbroker
      RETURN VARCHAR2;

   FUNCTION paysys_bm
      RETURN VARCHAR2;
   FUNCTION paysys_mplus
      RETURN VARCHAR2;

   FUNCTION id_request_yes
      RETURN VARCHAR2;
   FUNCTION id_request_no
      RETURN VARCHAR2;

   FUNCTION id_log_message_info
      RETURN bcp_log_message_type.log_level%TYPE;
   FUNCTION id_log_message_debug
      RETURN bcp_log_message_type.log_level%TYPE;
   FUNCTION id_log_message_warning
      RETURN bcp_log_message_type.log_level%TYPE;
   FUNCTION id_log_message_error
      RETURN bcp_log_message_type.log_level%TYPE;
   FUNCTION id_log_message_fatal
      RETURN bcp_log_message_type.log_level%TYPE;

   FUNCTION vip_client_yes
      RETURN VARCHAR2;
   FUNCTION vip_client_no
      RETURN VARCHAR2;

   gc_result_code_success       CONSTANT NUMBER := 0;

   FUNCTION has_doc_factura_by_role_47_yes
      RETURN VARCHAR2;
   FUNCTION has_doc_factura_by_role_47_no
      RETURN VARCHAR2;

   FUNCTION is_doc_factura_by_role_47_yes
      RETURN VARCHAR2;
   FUNCTION is_doc_factura_by_role_47_no
      RETURN VARCHAR2;

   -- ������� ��������� ������������� ���� � ������� YYYYMMDD
   FUNCTION gv_format_date_yyyymmdd
      RETURN VARCHAR2;
   -- ������� ��������� ������������� ���� � ������� YYYY-MM-DD
   FUNCTION gv_format_date_yyyy_mm_dd
      RETURN VARCHAR2;
   -- ������� ��������� ������������� ���� � ������� HH24:MI:SS
   FUNCTION gv_format_date_hh24_mi_ss
      RETURN VARCHAR2;
   -- ������� ��������� ������������� ���� � ������� HH24MISS
   FUNCTION gv_format_date_hh24miss
      RETURN VARCHAR2;
   -- ������� ��������� ������������� ���� � ������� YYYYMMDDHH24MISS
   FUNCTION gv_form_date_yyyymmddhh24miss
      RETURN VARCHAR2;

   gc_is_acc_support_yes        CONSTANT VARCHAR2(1) := 'Y';
   FUNCTION acc_support_yes
      RETURN VARCHAR2;
   gc_is_acc_support_no         CONSTANT VARCHAR2(1) := 'N';
   FUNCTION acc_support_no
      RETURN VARCHAR2;

   gc_object_id_account         CONSTANT NUMBER := 8;

   FUNCTION stp_var_sett_a
      RETURN VARCHAR2;
   FUNCTION stp_var_sett_m
      RETURN VARCHAR2;
   FUNCTION stp_var_sett_c
      RETURN VARCHAR2;

   FUNCTION format_to_datetime
      RETURN VARCHAR2;

   FUNCTION param_id_access_it
      RETURN bcp_params.id%TYPE;
   FUNCTION param_id_access_business
      RETURN bcp_params.id%TYPE;

  FUNCTION LOCAL_MFO
      RETURN  teller.bcpaycache.mfo_deb%type;

  FUNCTION PRINCE_PENS_OKPO_1
      RETURN varchar2 ;
  FUNCTION PRINCE_PENS_OKPO_2
      RETURN varchar2 ;

  FUNCTION ACTYPECODEERR
      RETURN cas.ca0201account.cc0201actypecode%type;
  FUNCTION DAYS_MAX
      RETURN  bcp_params.id%type;
  FUNCTION MFO_BLK_CODE
      RETURN number ;


   c_exists_docs_in_cache_yes   CONSTANT VARCHAR2(1) := 'Y';
   c_exists_docs_in_cache_no    CONSTANT VARCHAR2(1) := 'N';
END bcp5_constants_pkg;
/

