CREATE OR REPLACE PACKAGE regular_jobs_pkg 
/******************************************************************************
$Date:26.11.2020 14:02:48$
$Author:V.Kostevych$
$Id:regular_jobs_pkg.pks$
$Revision:28$
$HeadURL:https://nexus.test.kv.aval/repository/BCPay-Deliveries/BCPay-DB/BCPay-DB-vx.x.x_20201126.zip$
$JIRA:BR-1$
******************************************************************************/

AS
	/******************************************************************************
     NAME: REGULAR_JOBS_PKG
     PURPOSE: ��������� ����� ��� ������ � �������

          REVISIONS:
          Ver        Date        Author          Jira             Description
          ---------  ----------  --------------- ---------------- ------------------------------------
          1.0.0      18.09.2019  ������� �.�.                     1.�������� ������.
          7.0.0      20200217    VSilenko         LCY-29          2.modification the procedure create_one_minute_job
          ---------  ----------  ---------------  ------------------------------------
   ******************************************************************************/

	PROCEDURE create_one_minute_job
	(
		p_job_action IN VARCHAR2,
		p_job_name   IN VARCHAR2,
		p_err        OUT NUMBER, -- ��� ������
		p_err_txt    OUT VARCHAR2 -- ����� ������
	);

	PROCEDURE start_job(p_job_name IN VARCHAR2);

	PROCEDURE stop_job(p_job_name IN VARCHAR2);

	PROCEDURE run_job(p_job_name IN VARCHAR2);

END regular_jobs_pkg;
/

