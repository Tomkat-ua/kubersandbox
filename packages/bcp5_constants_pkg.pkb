create or replace package body BCP5_CONSTANTS_PKG 
/******************************************************************************
$Date:15.02.2021 19:42:33$
$Author:V.Kostevych$
$Id:bcp5_constants_pkg.pkb$
$Revision:58$
$HeadURL:https://nexus.test.kv.aval/repository/BCPay-Deliveries/BCPay-DB/BCPay-DB-vx.x.x_20210215.zip$
$JIRA:BR-2$
******************************************************************************/
-- 444

is

 /* *****************************************************************************
 NAME: TELLER.BCP5_CONSTANTS_PKG
      PURPOSE:    ����� ������ ���������

      REVISIONS:
      Ver        Date        Author           Jira              Description
      ---------  ----------  ---------------  ----------------  ------------------------------------
      4.2.0      20140508    OFrolyak         MIDASDEC-1765     1. Create this package.
      5.1.13     20151209    OFrolyak         SBCPAY-785        2. �������� ���������� STATUS_EVENT_ACC_FROM_PROC
      5.2.3      20160817    OFrolyak         DTASKS-4953       3. ������������� ���� USER_ROLE_47, USER_ROLE_52, USER_ROLE_48, USER_ROLE_49, USER_ROLE_51, user_role_88, USER_ROLE_79, user_role_78
                 20160822    OFrolyak         DTASKS-4953       4. ��������� role_group_teller, role_group_supervisor, role_group_report_manager, role_group_expert_its, role_group_manager_its
      5.3.3      20180420    OFrolyak         WMBT-2409         5. Added procedure doc_status_e, constants gc_batch_processing_yes, gc_batch_processing_no
      5.3.4      20191017    VKostevych       LCY-78            6. Add functions user_name_bp_checker,DOC_STATUS_I
      5.3.5      20191108    VKostevych       LCY-78            7. Add LOCAL_MFO,PRINCE_PENS_OKPO_1,PRINCE_PENS_OKPO_2,ACTYPECODEERR
      6.0.3      20191017    VKostevych       LCY-78,L�Y-117,120   Add functions user_name_bp_checker,doc_status_i
   ***************************************************************************** */

   FUNCTION N RETURN VARCHAR2 IS BEGIN RETURN 'N'; END;
   FUNCTION Y RETURN VARCHAR2 IS BEGIN RETURN 'Y'; END;

   FUNCTION DOC_STATUS_H RETURN VARCHAR2 IS BEGIN RETURN 'H'; END;
   FUNCTION DOC_STATUS_F RETURN VARCHAR2 IS BEGIN RETURN 'F'; END;
   FUNCTION DOC_STATUS_P RETURN VARCHAR2 IS BEGIN RETURN 'P'; END;
   FUNCTION DOC_STATUS_X RETURN VARCHAR2 IS BEGIN RETURN 'X'; END;
   FUNCTION DOC_STATUS_Z RETURN VARCHAR2 IS BEGIN RETURN 'Z'; END;
   FUNCTION DOC_STATUS_S RETURN VARCHAR2 IS BEGIN RETURN 'S'; END;
   FUNCTION DOC_STATUS_B RETURN VARCHAR2 IS BEGIN RETURN 'B'; END;
   FUNCTION DOC_STATUS_C RETURN VARCHAR2 IS BEGIN RETURN 'C'; END;
   FUNCTION DOC_STATUS_V RETURN VARCHAR2 IS BEGIN RETURN 'V'; END;
   FUNCTION DOC_STATUS_Y RETURN VARCHAR2 IS BEGIN RETURN 'Y'; END;
   FUNCTION DOC_STATUS_A RETURN VARCHAR2 IS BEGIN RETURN 'A'; END;
   FUNCTION DOC_STATUS_UNKNOWN RETURN VARCHAR2 IS BEGIN RETURN '#'; END;
   FUNCTION DOC_STATUS_D RETURN VARCHAR2 IS BEGIN RETURN 'D'; END;
   FUNCTION DOC_STATUS_E RETURN VARCHAR2 IS BEGIN RETURN 'E'; END;
   FUNCTION DOC_STATUS_I RETURN VARCHAR2 IS BEGIN RETURN 'I'; END;

   FUNCTION STATUS_EVENT_REM_FROM_PROC RETURN VARCHAR2 IS BEGIN RETURN 'REMOVE_DOC_FROM_PROC'; END;
   FUNCTION STATUS_EVENT_FACTURA_ROLE_47 RETURN VARCHAR2 IS BEGIN RETURN 'FACTURA_BY_ROLE_47'; END;
   FUNCTION STATUS_EVENT_ACC_TO_PROC RETURN VARCHAR2 IS BEGIN RETURN 'ACC_TO_PROC'; END;
   FUNCTION STATUS_EVENT_ACC_TO_PROC_REFIX RETURN VARCHAR2 IS BEGIN RETURN 'ACC_TO_PROC_REFIX'; END;
   FUNCTION STATUS_EVENT_ACC_TO_PROC_SUPP RETURN VARCHAR2 IS BEGIN RETURN 'ACC_TO_PROC_SUPP'; END;
   FUNCTION STATUS_EVENT_ACC_FROM_PROC RETURN VARCHAR2 IS BEGIN RETURN 'ACC_FROM_PROC'; END;

   FUNCTION USER_ROLE_18 RETURN VARCHAR2 IS BEGIN RETURN '18'; END;
   FUNCTION USER_ROLE_XX RETURN VARCHAR2 IS BEGIN RETURN 'XX'; END;
   --FUNCTION USER_ROLE_47 RETURN VARCHAR2 IS BEGIN RETURN '47'; END;
   --FUNCTION USER_ROLE_52 RETURN VARCHAR2 IS BEGIN RETURN '52'; END;
   --FUNCTION USER_ROLE_79 RETURN VARCHAR2 IS BEGIN RETURN '79'; END;
   --FUNCTION USER_ROLE_78 RETURN VARCHAR2 IS BEGIN RETURN '78'; END;
   --FUNCTION USER_ROLE_48 RETURN VARCHAR2 IS BEGIN RETURN '48'; END;
   --FUNCTION USER_ROLE_49 RETURN VARCHAR2 IS BEGIN RETURN '49'; END;
   --FUNCTION USER_ROLE_51 RETURN VARCHAR2 IS BEGIN RETURN '51'; END;
   --FUNCTION USER_ROLE_88 RETURN VARCHAR2 IS BEGIN RETURN '88'; END;

   FUNCTION role_group_teller RETURN NUMBER IS BEGIN RETURN 1; END;
   FUNCTION role_group_supervisor RETURN NUMBER IS BEGIN RETURN 2; END;
   FUNCTION role_group_report_manager RETURN NUMBER IS BEGIN RETURN 3; END;
   FUNCTION role_group_expert_its RETURN NUMBER IS BEGIN RETURN 4; END;
   FUNCTION role_group_manager_its RETURN NUMBER IS BEGIN RETURN 5; END;
   FUNCTION role_group_tech_bank_system RETURN NUMBER IS BEGIN RETURN 6; END;

   FUNCTION SYSTEM_ID_BCPAY_ONLINE RETURN NUMBER IS BEGIN RETURN 1; END;
   FUNCTION SYSTEM_ID_BCPAY_OFFLINE RETURN NUMBER IS BEGIN RETURN 2; END;
   FUNCTION SYSTEM_ID_BCPAY_STP RETURN NUMBER IS BEGIN RETURN 3; END;
   FUNCTION SYSTEM_ID_MBROKER RETURN NUMBER IS BEGIN RETURN 4; END;

   FUNCTION SYSTEM_MNEMO_BCPAY_ONLINE RETURN VARCHAR2 IS BEGIN RETURN 'BCPAY'; END;
   FUNCTION SYSTEM_MNEMO_BCPAY_OFFLINE RETURN VARCHAR2 IS BEGIN RETURN 'BCPAY-OFFLINE'; END;
   FUNCTION SYSTEM_MNEMO_BCPAY_STP RETURN VARCHAR2 IS BEGIN RETURN 'BCPAY-STP'; END;
   FUNCTION SYSTEM_MNEMO_MBROKER RETURN VARCHAR2 IS BEGIN RETURN 'MBROKER'; END;

   FUNCTION user_name_bp_checker RETURN VARCHAR2 IS BEGIN RETURN 'CHEK'; END;
   FUNCTION user_name_bp_mbroker RETURN VARCHAR2 IS BEGIN RETURN 'BROK'; END;
   FUNCTION user_role_bp_mbroker RETURN VARCHAR2 IS BEGIN RETURN '00'; END;

   FUNCTION PAYSYS_BM RETURN VARCHAR2 IS BEGIN RETURN 'BM'; END;
   FUNCTION PAYSYS_MPLUS RETURN VARCHAR2 IS BEGIN RETURN 'MPLUS'; END;

   FUNCTION ID_REQUEST_YES RETURN VARCHAR2 IS BEGIN RETURN 'Y'; END;
   FUNCTION ID_REQUEST_NO RETURN VARCHAR2 IS BEGIN RETURN 'N'; END;

   FUNCTION ID_LOG_MESSAGE_INFO RETURN bcp_log_message_type.log_level%TYPE IS BEGIN RETURN 'INFO'; END;
   FUNCTION ID_LOG_MESSAGE_DEBUG RETURN bcp_log_message_type.log_level%TYPE IS BEGIN RETURN 'DEBUG'; END;
   FUNCTION ID_LOG_MESSAGE_WARNING RETURN bcp_log_message_type.log_level%TYPE IS BEGIN RETURN 'WARN'; END;
   FUNCTION ID_LOG_MESSAGE_ERROR RETURN bcp_log_message_type.log_level%TYPE IS BEGIN RETURN 'ERROR'; END;
   FUNCTION ID_LOG_MESSAGE_FATAL RETURN bcp_log_message_type.log_level%TYPE IS BEGIN RETURN 'FATAL'; END;

   FUNCTION VIP_CLIENT_YES RETURN VARCHAR2 IS BEGIN RETURN 'Y'; END;
   FUNCTION VIP_CLIENT_NO RETURN VARCHAR2 IS BEGIN RETURN 'N'; END;

   FUNCTION HAS_DOC_FACTURA_BY_ROLE_47_YES RETURN VARCHAR2 IS BEGIN RETURN 'Y'; END;
   FUNCTION HAS_DOC_FACTURA_BY_ROLE_47_NO RETURN VARCHAR2 IS BEGIN RETURN 'N'; END;

   FUNCTION IS_DOC_FACTURA_BY_ROLE_47_YES RETURN VARCHAR2 IS BEGIN RETURN 'Y'; END;
   FUNCTION IS_DOC_FACTURA_BY_ROLE_47_NO RETURN VARCHAR2 IS BEGIN RETURN 'N'; END;

  -- --------------------------------------------------------------------------
  -- ������� ������ ���� � ������� YYYYMMDD
  -- --------------------------------------------------------------------------
  FUNCTION GV_FORMAT_DATE_YYYYMMDD RETURN VARCHAR2 IS BEGIN RETURN 'YYYYMMDD'; END;

  -- --------------------------------------------------------------------------
  -- ������� ������ ���� � ������� YYYY-MM-DD
  -- --------------------------------------------------------------------------
  FUNCTION GV_FORMAT_DATE_YYYY_MM_DD RETURN VARCHAR2 IS BEGIN RETURN 'YYYY-MM-DD'; END;

  -- --------------------------------------------------------------------------
  -- ������� ������ ���� � ������� HH24:MI:SS
  -- --------------------------------------------------------------------------
  FUNCTION GV_FORMAT_DATE_HH24_MI_SS RETURN VARCHAR2 IS BEGIN RETURN 'HH24:MI:SS'; END;

  -- --------------------------------------------------------------------------
  -- ������� ������ ���� � ������� HH24MISS
  -- --------------------------------------------------------------------------
  FUNCTION GV_FORMAT_DATE_HH24MISS RETURN VARCHAR2 IS BEGIN RETURN 'HH24MISS'; END;

  -- --------------------------------------------------------------------------
  -- ������� ������ ���� � ������� YYYYMMDDHH24MISS
  -- --------------------------------------------------------------------------
  FUNCTION GV_FORM_DATE_YYYYMMDDHH24MISS RETURN VARCHAR2 IS BEGIN RETURN 'YYYYMMDDHH24MISS'; END;

  -- --------------------------------------------------------------------------
  --
  -- --------------------------------------------------------------------------
  FUNCTION ACC_SUPPORT_YES RETURN VARCHAR2 IS BEGIN RETURN GC_IS_ACC_SUPPORT_YES; END;

  -- --------------------------------------------------------------------------
  --
  -- --------------------------------------------------------------------------
  FUNCTION ACC_SUPPORT_NO RETURN VARCHAR2 IS BEGIN RETURN GC_IS_ACC_SUPPORT_NO; END;

  FUNCTION STP_VAR_SETT_A RETURN VARCHAR2 IS BEGIN RETURN 'A'; END;
  FUNCTION STP_VAR_SETT_M RETURN VARCHAR2 IS BEGIN RETURN 'M'; END;
  FUNCTION STP_VAR_SETT_C RETURN VARCHAR2 IS BEGIN RETURN 'C'; END;

  -- --------------------------------------------------------------------------
  --
  -- --------------------------------------------------------------------------
  FUNCTION FORMAT_TO_DATETIME RETURN VARCHAR2 IS BEGIN RETURN 'DD-MON-RR HH.MI.SSXFF6 AM'; END;

  FUNCTION PARAM_ID_ACCESS_IT RETURN BCP_PARAMS.ID%TYPE IS BEGIN RETURN 'Access_IT'; END;
  FUNCTION PARAM_ID_ACCESS_BUSINESS RETURN BCP_PARAMS.ID%TYPE IS BEGIN RETURN 'Access_Business'; END;

  ------------------------------------------------------------------------------
  --��� ��
  FUNCTION LOCAL_MFO RETURN  teller.bcpaycache.mfo_deb%type IS BEGIN RETURN 380805; END;

  ------------------------------------------------------------------------------
  --��������, �� ������� ������� ���� ��������
  FUNCTION DAYS_MAX  RETURN  bcp_params.id%type  IS BEGIN RETURN 'count_days_max_process_doc'; END;

  ------------------------------------------------------------------------------
  --�������� ����������� ����
  FUNCTION PRINCE_PENS_OKPO_1 RETURN varchar2 IS BEGIN RETURN '0000000000'; END;
  FUNCTION PRINCE_PENS_OKPO_2 RETURN varchar2 IS BEGIN RETURN '99999'; END;

  ------------------------------------------------------------------------------
  --�������� ���� ���������� ������������� ���
  FUNCTION MFO_BLK_CODE RETURN number IS BEGIN RETURN 4 ; END;

  ------------------------------------------------------------------------------
  --��� ����� ������� - ����������� �� ��������
  FUNCTION ACTYPECODEERR RETURN cas.ca0201account.cc0201actypecode%type IS BEGIN RETURN 499; END;

end BCP5_CONSTANTS_PKG;
/

