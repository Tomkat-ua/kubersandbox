CREATE OR REPLACE PACKAGE bcp3_flow
/******************************************************************************
$Date:15.02.2021 19:31:58$
$Author:V.Kostevych$
$Id:bcp3_flow.pks$
$Revision:56$
$HeadURL:https://nexus.test.kv.aval/repository/BCPay-Deliveries/BCPay-DB/BCPay-DB-vx.x.x_20210215.zip$
$JIRA:BR-1$
$IsMaster: false$
******************************************************************************/
--ggghfghfgh
AS

   /* *****************************************************************************
      NAME:       BCP_ROBOT_XML_PKG
      PURPOSE:    ����� �������� � ���� �����������, ��� ���������� ��������,
                  � ������ 30-������� ������

      REVISIONS:
      Ver        Date        Author           Jira           Description
      ---------  ----------  ---------------  -------------  ------------------------------------
      5.3.0      20170908    OFrolyak         SBCPAY-855     1. Added procedure remove_old_logs
      6.0.2      20191004    VSilenko         LCY-92         2. ARCH_BCP4_DOC_PROC_ARCH_TBL exclude doc_id who exists in  bcpaycache
      6.0.5      20200115    VSilenko         LCY-147        3. Added procedure daily_archive_log
      6.0.10     20200630    VKostevych       LCY-146        6. Add procedure remove_old_logs_parts - remove old partitions BCP_LOG
      ***************************************************************************** */

   -- Procedure for move data to archive(bcp_doc_proc->bcp_doc_proc_arch)
   PROCEDURE arch_bcp_doc_proc;
   -- Procedure for move data to archive (BCP4_DOC_PROC_TBL->BCP4_DOC_PROC_ARCH_TBL)
   PROCEDURE arch_BCP4_DOC_PROC_ARCH_TBL;
   --
   PROCEDURE remove_old_logs;
   PROCEDURE remove_old_logs_parts  (p_err out varchar2 ,p_err_txt out varchar2);
   PROCEDURE daily_archive_log;

   PROCEDURE create_job_02hur
   (
      p_err        OUT NUMBER, -- ��� ������
      p_err_txt    OUT VARCHAR2 -- ����� ������
   );

END bcp3_flow;
/

