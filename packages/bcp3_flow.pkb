CREATE OR REPLACE PACKAGE BODY bcp3_flow
/******************************************************************************
$Date:15.02.2021 19:31:58$
$Author:V.Kostevych$
$Id:bcp3_flow.pkb$
$Revision:56$
$HeadURL:https://nexus.test.kv.aval/repository/BCPay-Deliveries/BCPay-DB/BCPay-DB-vx.x.x_20210215.zip$
$JIRA:BR-1$
   $IsMaster:$
******************************************************************************/
--5555555555  
AS

   /* *****************************************************************************
      NAME:       BCP_ROBOT_XML_PKG
      PURPOSE:    ����� �������� � ���� �����������, ��� ���������� ��������,
                  � ������ 30-������� ������

      REVISIONS:
      Ver        Date        Author           Jira           Description
      ---------  ----------  ---------------  -------------  ------------------------------------
      5.3.0      20170908    OFrolyak         SBCPAY-855     1. Added procedure remove_old_logs
      6.0.2      20191004    VSilenko         LCY-92         2. ARCH_BCP4_DOC_PROC_ARCH_TBL exclude doc_id who exists in  bcpaycache
      6.0.5      20200115    VSilenko         LCY-147        3. Added procedure daily_archive_log
      6.0.5      20200212    VSilenko         LCY-180        4. Procedure daily_archive_log - added archive for bcpaycacheerrors
      6.0.9      20200324    VSilenko         LCY-223        5. Procedure daily_archive_log - send_status_time without modify to archive
      6.0.10     20200630    VKostevych       LCY-146        6. Add procedure remove_old_logs_parts - remove old partitions BCP_LOG
      7.0.2      20200721    VSilenko         LCY-345        7. Edit procedure daily_archive_log - added field DT
      ***************************************************************************** */

   emyerror EXCEPTION;

   -- -----------------------------------------------------------------------------------------------
   -- Procedure for move data to archive(bcp_doc_proc->bcp_doc_proc_arch)
   -- -----------------------------------------------------------------------------------------------
   PROCEDURE arch_bcp_doc_proc
   IS
      v_max_id              NUMBER;
      v_move_record_count   NUMBER;
      v_session_id          NUMBER;
   BEGIN

      -- ��������� ����� ������
      v_session_id := BCP_SESSION_ID_SEQ.NEXTVAL;

      create_log_record ( P_DESCRIPTION   => '������� ����������� ������ ������� TELLER.BCP_DOC_PROC',
                          P_MESSAGE_TYPE  => BCP5_CONSTANTS_PKG.ID_LOG_MESSAGE_INFO,
                          p_session_id    => v_session_id );



      SELECT MAX (proc.id)
        INTO v_max_id
        FROM bcp_doc_proc proc
       WHERE proc.ctime < TRUNC (SYSDATE) - 10;

      INSERT INTO bcp_doc_proc_arch (ID,
                                            DOCUMENT_ID,
                                            STATUS,
                                            TELLERID,
                                            CTIME,
                                            SYSTEM_ID,
                                            STATUS_PREVIOUS,
                                            STATUS_NEW_SUPERVISOR,
                                            SUPERVISOR_ID,
                                            CTIME_SUPERVISOR,
                                            SYSTEM_ID_SUPERVISOR,
                                            STATUS_PREVIOUS_SUPERVISOR)
         SELECT ID,
                DOCUMENT_ID,
                STATUS,
                TELLERID,
                CTIME,
                SYSTEM_ID,
                STATUS_PREVIOUS,
                STATUS_NEW_SUPERVISOR,
                SUPERVISOR_ID,
                CTIME_SUPERVISOR,
                SYSTEM_ID_SUPERVISOR,
                STATUS_PREVIOUS_SUPERVISOR
           FROM bcp_doc_proc proc
          WHERE proc.id <= v_max_id;

      v_move_record_count := SQL%ROWCOUNT;

      DELETE FROM bcp_doc_proc proc
            WHERE proc.id <= v_max_id;

      COMMIT;
      create_log_record (
         P_DESCRIPTION   => 'ʳ���� ����������� ������ ������� TELLER.BCP_DOC_PROC: ���� ���������� '||v_move_record_count||' ������',
         P_MESSAGE_TYPE  => BCP5_CONSTANTS_PKG.ID_LOG_MESSAGE_INFO,
         p_session_id    => v_session_id );

   EXCEPTION
      WHEN OTHERS
      THEN
         create_log_record (
            P_DESCRIPTION   => '������� ��� ����������� ������ ������� TELLER.BCP_DOC_PROC: ' || SQLERRM,
            P_MESSAGE_TYPE  => BCP5_CONSTANTS_PKG.ID_LOG_MESSAGE_ERROR,
            p_session_id    => v_session_id );
         DBMS_OUTPUT.put_line (SQLERRM);
         ROLLBACK;
   END;

   -- -----------------------------------------------------------------------------------------------
   -- Procedure for move data to archive (BCP4_DOC_PROC_TBL->BCP4_DOC_PROC_ARCH_TBL)
   -- -----------------------------------------------------------------------------------------------
   PROCEDURE ARCH_BCP4_DOC_PROC_ARCH_TBL
   IS
      v_min_id              NUMBER;
      v_move_record_count   NUMBER;
      v_move_record_count_all   NUMBER;
      v_batch_size          NUMBER;
      v_session_id          NUMBER;
   BEGIN

      v_batch_size := 5000;

      --
      v_move_record_count_all := 0;

      -- ��������� ����� ������
      v_session_id := BCP_SESSION_ID_SEQ.NEXTVAL;

      create_log_record ( P_DESCRIPTION   => 'BCP3_FLOW.ARCH_BCP4_DOC_PROC_ARCH_TBL: ������� ����������� ������ ������� TELLER.BCP4_DOC_PROC_ARCH_TBL',
                          P_MESSAGE_TYPE  => BCP5_CONSTANTS_PKG.ID_LOG_MESSAGE_INFO,
                          p_session_id    => v_session_id );

      --
      v_move_record_count := 1;

      --
      WHILE v_move_record_count > 0
      LOOP

         SELECT              MIN (proc.id)
         INTO                v_min_id
         FROM                BCP4_DOC_PROC_TBL proc;

         INSERT INTO BCP4_DOC_PROC_ARCH_TBL (
            ID,
            DOC_ID,
            STATUS_OLD,
            STATUS_NEW,
            USER_ID,
            USER_ROLE,
            UNIQUE_NUMBER_BY_DOC,
            COMMENTS,
            CTIME,
            CBO_ID,
            SYSTEM_ID )
         SELECT
            ID,
            DOC_ID,
            STATUS_OLD,
            STATUS_NEW,
            USER_ID,
            USER_ROLE,
            UNIQUE_NUMBER_BY_DOC,
            COMMENTS,
            CTIME,
            CBO_ID,
            SYSTEM_ID
         FROM BCP4_DOC_PROC_TBL proc
         WHERE proc.id <= v_min_id + v_batch_size
               AND proc.ctime < TRUNC (SYSDATE) - 31
               AND proc.doc_id NOT IN (SELECT c.id FROM bcpaycache c);

         v_move_record_count := SQL%ROWCOUNT;
         v_move_record_count_all := v_move_record_count_all + v_move_record_count;

      DELETE FROM bcp4_doc_proc_tbl proc
      WHERE  proc.id <= v_min_id + v_batch_size
           AND proc.ctime < trunc(SYSDATE) - 31
           AND proc.doc_id NOT IN (SELECT c.id FROM bcpaycache c);
         COMMIT;

         create_log_record (
            P_DESCRIPTION   => 'BCP3_FLOW.ARCH_BCP4_DOC_PROC_ARCH_TBL: ��������� �������� ����������� ������ ������� TELLER.BCP4_DOC_PROC_ARCH_TBL: ���� ���������� '||v_move_record_count||' ������',
            P_MESSAGE_TYPE  => BCP5_CONSTANTS_PKG.ID_LOG_MESSAGE_INFO,
            p_session_id    => v_session_id );

         -- ��������� ������ ������� ������� � 07.00 ����
         IF ( SYSDATE > TRUNC (SYSDATE) + 7/24 ) THEN
            create_log_record ( P_DESCRIPTION   => 'BCP3_FLOW.ARCH_BCP4_DOC_PROC_ARCH_TBL: ������� ������� ������� �������. �������� �����������.',
                          P_MESSAGE_TYPE  => BCP5_CONSTANTS_PKG.ID_LOG_MESSAGE_INFO,
                          p_session_id    => v_session_id );

            -- ������� �� �����
            EXIT;
         END IF;

      END LOOP;

      create_log_record ( P_DESCRIPTION   => 'BCP3_FLOW.ARCH_BCP4_DOC_PROC_ARCH_TBL: ��������� ����������� ������ ������� TELLER.BCP4_DOC_PROC_ARCH_TBL: ���� ���������� '||v_move_record_count_all||' ������',
                          P_MESSAGE_TYPE  => BCP5_CONSTANTS_PKG.ID_LOG_MESSAGE_INFO,
                          p_session_id    => v_session_id );

   EXCEPTION
      WHEN OTHERS
      THEN
         create_log_record (
            P_DESCRIPTION   => 'BCP3_FLOW.ARCH_BCP4_DOC_PROC_ARCH_TBL: ������� ��� ����������� ������ ������� TELLER.BCP4_DOC_PROC_ARCH_TBL: ' || SQLERRM,
            P_MESSAGE_TYPE  => BCP5_CONSTANTS_PKG.ID_LOG_MESSAGE_ERROR,
            p_session_id    => v_session_id );
         DBMS_OUTPUT.put_line (SQLERRM);

         ROLLBACK;
   END;

   -- -----------------------------------------------------------------------------------------------
   --
   -- -----------------------------------------------------------------------------------------------
   PROCEDURE remove_old_logs_parts
   (
      p_err     OUT VARCHAR2,
      p_err_txt OUT VARCHAR2
   ) IS
      v_session_id         NUMBER;
      partdate             TIMESTAMP;
      deldate              DATE := trunc(SYSDATE) -
                                   teller.bcp_tools.get_bcp_param_number('time_shift_for_remove_old_logs');
      sql_str              VARCHAR2(1000);
      v_remove_parts_count NUMBER;
   BEGIN
      -- ��������� ����� ������
      v_session_id := bcp_session_id_seq.nextval;

      create_log_record(p_description  => 'BCP3_FLOW.remove_old_logs_parts: ������� ��������� ������ ������� TELLER.BCP_LOG',
                        p_message_type => bcp5_constants_pkg.id_log_message_info,
                        p_session_id   => v_session_id);
      v_remove_parts_count := 0;
      FOR c IN (SELECT partition_name, high_value
                FROM   all_tab_partitions
                WHERE  table_name = 'BCP_LOG'
                       AND partition_name <> 'CTIME_P0')
      LOOP
         EXECUTE IMMEDIATE 'BEGIN :dt := ' || c.high_value || '; END;'
            USING OUT partdate;
         IF partdate < deldate
         THEN
            sql_str := 'alter table BCP_LOG drop partition ' || c.partition_name;
            EXECUTE IMMEDIATE sql_str;
            v_remove_parts_count := v_remove_parts_count + 1;
            create_log_record(p_description  => 'BCP3_FLOW.remove_old_logs_parts: ��������� �������� ��������� ������ ������� TELLER.BCP_LOG: ���� �������� �������� ' ||
                                                c.partition_name || ' �� ' || partdate,
                              p_message_type => bcp5_constants_pkg.id_log_message_info,
                              p_session_id   => v_session_id);
         END IF;
      END LOOP;
      p_err := '0';
      IF v_remove_parts_count > 0
      THEN
         p_err_txt := '������ �������� ������� ���� BCP_LOG, ������� �������� - ' ||
                      v_remove_parts_count;
      ELSE
         p_err_txt := '�� �������� ��������� ����� ';
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         p_err     := '1';
         p_err_txt := '������� ��������� ��������� ����� BCP_LOG: ' || SQLERRM;
         create_log_record(p_description  => 'BCP3_FLOW.remove_old_logs_parts: ������� ��� ����������� ������ ������� TELLER.BCP_LOG: ' ||
                                             SQLERRM,
                           p_message_type => bcp5_constants_pkg.id_log_message_error,
                           p_session_id   => v_session_id);
         ROLLBACK;
   END remove_old_logs_parts;

   PROCEDURE remove_old_logs
   IS
      v_err        varchar2(5);
      v_err_txt    varchar2(4000);
   BEGIN

     remove_old_logs_parts(v_err,v_err_txt);

     if v_err <> '0'
       then
            create_log_record (
            P_DESCRIPTION   => 'BCP3_FLOW.remove_old_logs: ' || v_err_txt,
            P_MESSAGE_TYPE  => BCP5_CONSTANTS_PKG.ID_LOG_MESSAGE_ERROR,
            p_session_id    => BCP_SESSION_ID_SEQ.NEXTVAL);
     end if;

   END;

   -- -----------------------------------------------------------------------------------------------
   -- ���������� ��������� ���������� �� bcpaycache � bcp4_doc_proc_tbl
   -- -----------------------------------------------------------------------------------------------
  PROCEDURE daily_archive_log IS
    TYPE tt_arr_doc IS TABLE OF bcpaycache% ROWTYPE INDEX BY PLS_INTEGER;
    p_arr_doc tt_arr_doc;
    c_proc CONSTANT VARCHAR2(30) := 'daily_archive_log';
  BEGIN
    create_log_record(p_description  => c_proc || ': ����� ��������� ����������',
                p_message_type => bcp5_constants_pkg.id_log_message_info,
                p_session_id   => bcp_session_id_seq.nextval);

    --�������� ���������, ���������� ���������
      SELECT *
    BULK   COLLECT
    INTO   p_arr_doc
    FROM   bcpaycache b
    WHERE  b.status IN ('S', 'F')
         AND b.dt < trunc(SYSDATE);

    --��������� ��������� � �����
      FORALL i IN p_arr_doc.first .. p_arr_doc.last
      INSERT INTO bcpaycache_arch
        (id,
         channel_id,
         session_id,
         batch_id,
         user_id,
         dealtypecode,
         status,
         paysysid,
         amount,
         currency,
         acc_deb,
         acc_cr,
         lacc_deb,
         lacc_cr,
         shorttext,
         longtext,
         symbol,
         form1pb,
         mfo_deb,
         mfo_cr,
         okpo_deb,
         okpo_cr,
         name_deb,
         name_cr,
         request,
         doctype,
         srcdocid,
         srcdocnum,
         attributes,
         externid,
         externsysid,
         srcdate,
         paysys,
         signature,
         stp_var$sett,
         stp_reas$rej,
         branch,
         oper_id,
         sys_deb_cur_isocode,
         sys_cr_cur_isocode,
         acc_name,
         status_name,
         mfo_deb_name,
         mfo_cr_name,
         branch_deb,
         branch_cr,
         sys_cln_deb_name,
         sys_cln_cr_name,
         sys_cln_code_deb,
         sys_cln_code_cr,
         sys_clientum_deb,
         sys_clientum_cr,
         user_in_charge_deb,
         user_in_charge_cr,
         acc_branch_name_deb,
         acc_branch_name_cr,
         region_name_deb,
         region_name_cr,
         paysys_time,
         paysys_ttl,
         datetime_ins,
         datetime_upd,
         datetime_upd_status,
         send_status_time,
         autoproc,
         region_code_deb,
         region_code_cr,
         teller_id,
         bcpaycache_id,
         teller_name,
         is_corporate,
         remove_from_processing,
         datetime_endprocess,
         client_doc,
         recipient_doc,
         is_receipter_internal,
         remove_from_processing_ctime,
         remove_from_processing_user,
         remove_from_processing_reason,
         payment_date,
         count_workdays_to_autopay,
         counter_workdays,
         rtime_counter_workdays,
         value_date,
         is_no_money,
         is_no_money_rtime,
         is_no_money_ticket,
         is_no_money_ticket_rtime,
         is_no_money_time_to_process,
         iban_deb,
         iban_cr,
         dt)
      VALUES
        (id_bcpaycache_arch.nextval,
         p_arr_doc(i).channel_id,
         p_arr_doc(i).session_id,
         p_arr_doc(i).batch_id,
         p_arr_doc(i).user_id,
         p_arr_doc(i).dealtypecode,
         p_arr_doc(i).status,
         p_arr_doc(i).paysysid,
         p_arr_doc(i).amount,
         p_arr_doc(i).currency,
         p_arr_doc(i).acc_deb,
         p_arr_doc(i).acc_cr,
         p_arr_doc(i).lacc_deb,
         p_arr_doc(i).lacc_cr,
         p_arr_doc(i).shorttext,
         p_arr_doc(i).longtext,
         p_arr_doc(i).symbol,
         p_arr_doc(i).form1pb,
         p_arr_doc(i).mfo_deb,
         p_arr_doc(i).mfo_cr,
         p_arr_doc(i).okpo_deb,
         p_arr_doc(i).okpo_cr,
         p_arr_doc(i).name_deb,
         p_arr_doc(i).name_cr,
         p_arr_doc(i).request,
         p_arr_doc(i).doctype,
         p_arr_doc(i).srcdocid,
         p_arr_doc(i).srcdocnum,
         p_arr_doc(i).attributes,
         p_arr_doc(i).externid,
         p_arr_doc(i).externsysid,
         p_arr_doc(i).srcdate,
         p_arr_doc(i).paysys,
         p_arr_doc(i).signature,
         p_arr_doc(i).stp_var$sett,
         p_arr_doc(i).stp_reas$rej,
         p_arr_doc(i).branch,
         p_arr_doc(i).oper_id,
         p_arr_doc(i).sys_deb_cur_isocode,
         p_arr_doc(i).sys_cr_cur_isocode,
         p_arr_doc(i).acc_name,
         p_arr_doc(i).status_name,
         p_arr_doc(i).mfo_deb_name,
         p_arr_doc(i).mfo_cr_name,
         p_arr_doc(i).branch_deb,
         p_arr_doc(i).branch_cr,
         p_arr_doc(i).sys_cln_deb_name,
         p_arr_doc(i).sys_cln_cr_name,
         p_arr_doc(i).sys_cln_code_deb,
         p_arr_doc(i).sys_cln_code_cr,
         p_arr_doc(i).sys_clientum_deb,
         p_arr_doc(i).sys_clientum_cr,
         p_arr_doc(i).user_in_charge_deb,
         p_arr_doc(i).user_in_charge_cr,
         p_arr_doc(i).acc_branch_name_deb,
         p_arr_doc(i).acc_branch_name_cr,
         p_arr_doc(i).region_name_deb,
         p_arr_doc(i).region_name_cr,
         p_arr_doc(i).paysys_time,
         p_arr_doc(i).paysys_ttl,
         p_arr_doc(i).datetime_ins,
         p_arr_doc(i).datetime_upd,
         p_arr_doc(i).datetime_upd_status,
         p_arr_doc(i).send_status_time,
         p_arr_doc(i).autoproc,
         p_arr_doc(i).region_code_deb,
         p_arr_doc(i).region_code_cr,
         p_arr_doc(i).teller_id,
         p_arr_doc(i).id,
         p_arr_doc(i).teller_name,
         p_arr_doc(i).is_corporate,
         p_arr_doc(i).remove_from_processing,
         p_arr_doc(i).datetime_endprocess,
         p_arr_doc(i).client_doc,
         p_arr_doc(i).recipient_doc,
         p_arr_doc(i).is_receipter_internal,
         p_arr_doc(i).remove_from_processing_ctime,
         p_arr_doc(i).remove_from_processing_user,
         p_arr_doc(i).remove_from_processing_reason,
         p_arr_doc(i).payment_date,
         p_arr_doc(i).count_workdays_to_autopay,
         p_arr_doc(i).counter_workdays,
         p_arr_doc(i).rtime_counter_workdays,
         p_arr_doc(i).value_date,
         p_arr_doc(i).is_no_money,
         p_arr_doc(i).is_no_money_rtime,
         p_arr_doc(i).is_no_money_ticket,
         p_arr_doc(i).is_no_money_ticket_rtime,
         p_arr_doc(i).is_no_money_time_to_process,
         p_arr_doc(i).iban_deb,
         p_arr_doc(i).iban_cr,
         p_arr_doc(i).dt);
    create_log_record(p_description  => c_proc || ': ���������� ������� � bcpaycache_arch:' ||
                            SQL%ROWCOUNT,
                p_message_type => bcp5_constants_pkg.id_log_message_info,
                p_session_id   => bcp_session_id_seq.nextval);

    --������� ������������ ��������� �� ����������� �������
      FORALL i IN p_arr_doc.first .. p_arr_doc.last
      DELETE FROM bcpaycache b WHERE b.id = p_arr_doc(i).id;
    create_log_record(p_description  => c_proc || ': ������� ������� �� bcpaycache:' ||
                            SQL%ROWCOUNT,
                p_message_type => bcp5_constants_pkg.id_log_message_info,
                p_session_id   => bcp_session_id_seq.nextval);

    --��������� � ����� ������� �������� ����������
      FORALL i IN p_arr_doc.first .. p_arr_doc.last
      INSERT INTO bcp4_doc_proc_arch_tbl
        (id,
         doc_id,
         status_old,
         status_new,
         user_id,
         user_role,
         unique_number_by_doc,
         comments,
         ctime,
         cbo_id,
         system_id)
        SELECT id,
             doc_id,
             status_old,
             status_new,
             user_id,
             user_role,
             unique_number_by_doc,
             comments,
             ctime,
             cbo_id,
             system_id
        FROM   bcp4_doc_proc_tbl t
        WHERE  t.doc_id = p_arr_doc(i).id;
    create_log_record(p_description  => c_proc ||
                            ': ���������� ������� � BCP4_DOC_PROC_ARCH_TBL:' ||
                            SQL%ROWCOUNT,
                p_message_type => bcp5_constants_pkg.id_log_message_info,
                p_session_id   => bcp_session_id_seq.nextval);

    --������� ������� �������� ���������� �� ����������� �������
      FORALL i IN p_arr_doc.first .. p_arr_doc.last
      DELETE FROM bcp4_doc_proc_tbl t WHERE t.doc_id = p_arr_doc(i).id;
    create_log_record(p_description  => c_proc || ': ������� ������� �� BCP4_DOC_PROC_TBL:' ||
                            SQL%ROWCOUNT,
                p_message_type => bcp5_constants_pkg.id_log_message_info,
                p_session_id   => bcp_session_id_seq.nextval);


      --��������� ������� bcpaycacheerrors
      FORALL i IN p_arr_doc.first .. p_arr_doc.last
        insert into bcpaycacheerrors_arch
        (
               id,
               cache_id,
               error_code,
               error_descr
        )
        select
               ID_BCPAYCACHE_ERROR_ARCH.Nextval,
               p_arr_doc(i).id,
               bce.error_code,
               bce.error_descr
        from bcpaycacheerrors bce where bce.cache_id = p_arr_doc(i).id;
      create_log_record(p_description  => c_proc || ': ���������� ������� � BCPAYCACHEERRORS_ARCH:' ||
                                          SQL%ROWCOUNT,
                        p_message_type => bcp5_constants_pkg.id_log_message_info,
                        p_session_id   => bcp_session_id_seq.nextval);

      --������� ������� ������ �� ����������
      FORALL i IN p_arr_doc.first .. p_arr_doc.last
         DELETE FROM bcpaycacheerrors t WHERE t.cache_id = p_arr_doc(i).id;
      create_log_record(p_description  => c_proc || ': ������� ������� �� BCPAYCACHEERRORS:' ||
                                          SQL%ROWCOUNT,
                        p_message_type => bcp5_constants_pkg.id_log_message_info,
                        p_session_id   => bcp_session_id_seq.nextval);

    COMMIT;

      create_log_record(p_description  => c_proc || ': ���������� ��������� ����������',
                p_message_type => bcp5_constants_pkg.id_log_message_info,
                p_session_id   => bcp_session_id_seq.nextval);

  EXCEPTION
    WHEN OTHERS THEN
      create_log_record(p_description  => c_proc || ': ������ ��������� ����������: ' || SQLERRM,
                  p_message_type => bcp5_constants_pkg.id_log_message_error,
                  p_session_id   => bcp_session_id_seq.nextval);
  END daily_archive_log;

   PROCEDURE create_job_02hur
   (
      p_err        OUT NUMBER, -- ��� ������
      p_err_txt    OUT VARCHAR2 -- ����� ������
   ) IS
      v_cnt NUMBER;
      p_job_action  VARCHAR2(100) := 'bcp3_flow.daily_archive_log';
      p_job_name    VARCHAR2(30) := 'DAILY_ARCHIVE_LOG_JOB';
      c_proc CONSTANT VARCHAR2(30) := 'create_job_02hur';

   BEGIN
      p_err     := 0;
      p_err_txt := 'Ok';

      SELECT COUNT(1) INTO v_cnt FROM user_scheduler_jobs j WHERE j.job_name = p_job_name;
      IF v_cnt <> 0
      THEN
         p_err_txt := 'Job ' || p_job_name || ' already exists';
         RAISE emyerror;
      END IF;

      dbms_scheduler.create_job(job_name        => p_job_name,
                                job_type        => 'STORED_PROCEDURE',
                                job_action      => p_job_action,
                                start_date      => trunc(sysdate)+2/24,
                                repeat_interval => 'FREQ=DAILY;INTERVAL=1',
                                end_date        => NULL,
                                enabled         => TRUE);
   EXCEPTION
      WHEN emyerror THEN
         p_err := -1;
      WHEN OTHERS THEN
         p_err     := -1;
         p_err_txt := SQLERRM;
         create_log_record(p_description  => c_proc || ': ������ ������� �����: ' || p_job_name || ' Error:' || p_err_txt,
                           p_message_type => bcp5_constants_pkg.id_log_message_error,
                           p_session_id   => bcp_session_id_seq.nextval);

         RAISE;
   END;


END bcp3_flow;
/

