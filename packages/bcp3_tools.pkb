create or replace package body bcp3_tools
/******************************************************************************
$Date:15.02.2021 19:40:24$
$Author:V.Kostevych$
$Id:bcp3_tools.pkb$
$Revision:57$
$HeadURL:https://nexus.test.kv.aval/repository/BCPay-Deliveries/BCPay-DB/BCPay-DB-vx.x.x_20210215.zip$
$JIRA:BR-2$
******************************************************************************/
--23423245
-- add change 1  BR-2
as

/* *****************************************************************************
NAME:       bcp3_tools
PURPOSE:

REVISIONS:
Ver        Date        Author           Jira           Description
---------  ----------  ---------------  -------------  ------------------------------------
5.1.7      20150910    OFrolyak                        1. ������� ��������� � ��������� str_timestamp2timestamp
5.1.13     20151208    OFrolyak                        2. ������� ��������� � ��������� CLEAN_NOTPRINTED_SYMBOLS
5.2.4      20160912    OFrolyak         SBCPAY-797     3. �������������� ��������� is_delay_doc_corp
5.2.5      20161010    OFrolyak                        4. ������� ��������� � ��������� get_meth_disp_acc_balance, init_log_level (added hint RESULT_CACHE)

***************************************************************************** */

-- ���, ��� ���������� ������������� ������ �� ������� �����������
type table_log_level is table of BCP_LOG_MESSAGE_TYPE.is_save%type
index by BCP_LOG_MESSAGE_TYPE.log_level%type;
-- ������������� ������ �� ������� �����������
log_level_now   table_log_level;

-- --------------------------------------------------------------------------- --
-- ���������, ��� ����������������� ������������� ������ �� ������� �����������
-- --------------------------------------------------------------------------- --
procedure init_log_level
is
begin
for rec in (select /*+ RESULT_CACHE */ l.log_level,
l.is_save
from BCP_LOG_MESSAGE_TYPE l)
loop
log_level_now(rec.log_level) := rec.is_save;
end loop;
end;

-- --------------------------------------------------------------------------- --
-- �������� ������ � ������� ����� LOG
-- �����������: ������������ ��������� ������� ����������� �� ������� �. LOG_LEVEL
-- --------------------------------------------------------------------------- --
procedure to_log(        p_description  in varchar2,
p_message_type in VARCHAR2,
p_session_id   IN NUMBER )
is
begin

-- ���� �� ���������� - ������ �������
if (not log_level_now(p_message_type) = 'Y') then
return;
end if;

-- ������� ������ � ������� �����������
INSERT INTO bcp_log( id, description, message_type, session_id )
VALUES ( id_bcp_log.nextval, p_description, p_message_type, p_session_id );

end;

-- -------------------------------------------------------------------------------------------
-- ������� ������ ������� �������� ��� �������: �� - 1, �+ - 0
-- -------------------------------------------------------------------------------------------
function get_system_id_by_paysys(str in varchar2)
return number
is
res    number;
begin
if upper(trim(str)) = 'BM' then
res := 1;
else
res := 0;
end if;

return res;

end get_system_id_by_paysys;

-- -------------------------------------------------------------------------------------------
-- �������� ���� ���������, �� �������� ������, �� ��������� �������
-- (��� ������������ ���� ���� ��� ���������� ������� �� �������)
-- � ����������� ���, ���� ��������� �������.
-- -------------------------------------------------------------------------------------------
-- ������� ��������� ������ ����������� ������� � ��������� �� ��� �� ������� BCPAY
function get_meth_disp_acc_balance(
p_role            in varchar2, -- ����
p_bcp_system_char in varchar2
)
return number
is
m_meth_disp_acc_balance   number; -- ������������� ������ ����������� �������
bcp_system_id             number;
begin
-- �������� �� ����������� ������� BCPay
begin
select /*+ RESULT_CACHE */ bcp_system.id
into   bcp_system_id
from   bcp_systems bcp_system
where  bcp_system.mnemo = p_bcp_system_char;
exception
when no_data_found then
begin
select 1 into m_meth_disp_acc_balance from dual;
return m_meth_disp_acc_balance;
end;
end;
-- ���� �������� ���� = ����
if p_role is null then
/*
select bcp_role.display_method_acc_balance
into m_meth_disp_acc_balance
from bcp_roles bcp_role,
bcp_systems bcp_system
where bcp_system.mnemo = p_bcp_system_char
and bcp_system.id = bcp_role.bcp_system_id
and bcp_role.role in null;
*/
select /*+ RESULT_CACHE */ bcp_role.display_method_acc_balance
into   m_meth_disp_acc_balance
from   bcp_roles bcp_role
where  bcp_role.role is null
and bcp_role.bcp_system_id = ( select /*+ RESULT_CACHE */ bcp_system.id
from   bcp_systems bcp_system
where  bcp_system.mnemo = p_bcp_system_char );
elsif p_role is not null then
-- ��������� ������������� ������ ����������� �������
/*
select bcp_role.display_method_acc_balance
into m_meth_disp_acc_balance
from bcp_roles bcp_role,
bcp_systems bcp_system
where bcp_system.mnemo = p_bcp_system_char
and bcp_system.id = bcp_role.bcp_system_id
and bcp_role.role = p_role;
*/
select /*+ RESULT_CACHE */ bcp_role.display_method_acc_balance
into   m_meth_disp_acc_balance
from   bcp_roles bcp_role
where  bcp_role.role = p_role
and bcp_role.bcp_system_id = ( select /*+ RESULT_CACHE */ bcp_system.id
from   bcp_systems bcp_system
where  bcp_system.mnemo = p_bcp_system_char );
end if;

return m_meth_disp_acc_balance;
exception
when no_data_found then
begin
-- ��������� ������������� ������ ����������� �������,
-- ���� ��������� ������������� ��� �� ������������� � "������" BCP_DISPLAY_ACCOUNT_BALANCE
/*
select bcp_role.display_method_acc_balance
into m_meth_disp_acc_balance
from bcp_roles bcp_role,
bcp_systems bcp_system
where bcp_system.mnemo = p_bcp_system_char
and bcp_system.id = bcp_role.bcp_system_id
and bcp_role.role = '00';
*/
select /*+ RESULT_CACHE */ bcp_role.display_method_acc_balance
into   m_meth_disp_acc_balance
from   bcp_roles bcp_role
where  bcp_role.role = '00'
and bcp_role.bcp_system_id = ( select /*+ RESULT_CACHE */ bcp_system.id
from   bcp_systems bcp_system
where  bcp_system.mnemo = p_bcp_system_char );
return m_meth_disp_acc_balance;
end;
end;

-- -----------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------
/*
function get_pending_docs_amt_with_role(
p_account         in varchar2, -- ����� �������
p_account_ccy     in varchar2, -- ������ �������
p_role            in varchar2, -- ����
p_bcp_system_char in varchar2
)
return number -- ���� ��������� "� �������" �� ����� ������� (� �������)
is
m_amount                   number(16) := 0; -- ���� ���������
m_amount_autopay           number(16) := 0; -- ���� ���������, ����������� �� ����������
m_amount_doc_for_control   number(16) := 0; -- ���� ���������, ����������� �� ����������
m_meth_disp_acc_balance    number; -- ������������� ������ ����������� �������
begin
-- ��������� ������������� ������ ����������� �������
m_meth_disp_acc_balance :=
get_meth_disp_acc_balance(p_role, p_bcp_system_char);

-- ���������, �� ���������� �� ����������� ������
select nvl(sum(amount), 0)
into m_amount_autopay
--        from teller.bcp3_doc docs
from teller.bcp3_doc_pending_docs docs
--       where docs.status in ('P', 'E') -- ������ - ����� ������
--       where docs.status in ('P', 'E', 'A') -- ������ - ����� ������
where docs.status in ('P', 'E', 'A', 'Y') -- ������ - ����� ������
and ((docs.acc_deb = p_account
and docs.ccy = p_account_ccy
and docs.request = 'N')
or  (docs.acc_cr = p_account
and docs.ccy = p_account_ccy
and docs.request = 'Y'));

case
when m_meth_disp_acc_balance = 1 then
begin
-- ����������� ����
select 0 into m_amount from dual;
end;
when m_meth_disp_acc_balance = 2 then
begin
-- ����������� ����
select m_amount_autopay into m_amount from dual;
end;
when m_meth_disp_acc_balance = 3 then
begin
-- ���� �������
null;
end;
when m_meth_disp_acc_balance = 4 then
begin
-- ���������, �� ���������� �� ���������� �� �����²����
select nvl(sum(docs.amount), 0)
into m_amount_doc_for_control
from teller.bcp3_doc docs,
teller.bcp_doc_for_control doc_for_control
where doc_for_control.document_id = docs.id
and ((docs.acc_deb = p_account
and docs.ccy = p_account_ccy
and docs.request = 'N')
or  (docs.acc_cr = p_account
and docs.ccy = p_account_ccy
and docs.request = 'Y'));

-- ����������� ����
select m_amount_autopay + m_amount_doc_for_control
into m_amount
from dual;
end;
end case;

return m_amount;
end;
*/
function get_pending_docs_amt_with_role(
p_account         in varchar2, -- ����� �������
p_account_ccy     in varchar2, -- ������ �������
p_role            in varchar2, -- ����
p_bcp_system_char in varchar2
)
return number -- ���� ��������� "� �������" �� ����� ������� (� �������)
is
m_amount                   number(16) := 0; -- ���� ���������
m_amount_autopay           number(16) := 0; -- ���� ���������, ����������� �� ����������
m_amount_doc_for_control   number(16) := 0; -- ���� ���������, ����������� �� ����������
m_meth_disp_acc_balance    number; -- ������������� ������ ����������� �������
begin
-- ��������� ������������� ������ ����������� �������
m_meth_disp_acc_balance :=
get_meth_disp_acc_balance(p_role, p_bcp_system_char);

-- ���������, �� ���������� �� ����������� ������
select nvl(sum(amount), 0)
into m_amount_autopay
--        from teller.bcp3_doc docs
from teller.bcp3_doc_pending_docs docs
--       where docs.status in ('P', 'E') -- ������ - ����� ������
--       where docs.status in ('P', 'E', 'A') -- ������ - ����� ������
where docs.status in ('P', 'E', 'A', 'Y') -- ������ - ����� ������
and docs.acc_deb = p_account
and docs.ccy = p_account_ccy
and docs.request = 'N';

case
when m_meth_disp_acc_balance = 1 then
begin
-- ����������� ����
select 0 into m_amount from dual;
end;
when m_meth_disp_acc_balance = 2 then
begin
-- ����������� ����
select m_amount_autopay into m_amount from dual;
end;
when m_meth_disp_acc_balance = 3 then
begin
-- ���� �������
null;
end;
when m_meth_disp_acc_balance = 4 then
begin
-- ���������, �� ���������� �� ���������� �� �����²����
select nvl(sum(docs.amount), 0)
into m_amount_doc_for_control
from teller.bcp3_doc docs,
teller.bcp_doc_for_control doc_for_control
where doc_for_control.document_id = docs.id
and docs.acc_deb = p_account
and docs.ccy = p_account_ccy
and docs.request = 'N';

-- ����������� ����
select m_amount_autopay + m_amount_doc_for_control
into m_amount
from dual;
end;
end case;

return m_amount;
end;


/*
function str_timestamp2timestamp(str in varchar2)
return timestamp
is
result   timestamp;
format   varchar2(100);
begin

execute immediate 'ALTER SESSION SET NLS_LANGUAGE=AMERICAN';
execute immediate 'ALTER SESSION SET NLS_TERRITORY=AMERICA';

begin
select NDP.VALUE into format from NLS_DATABASE_PARAMETERS NDP where  NDP.PARAMETER = 'NLS_TIMESTAMP_FORMAT';
if INSTR(str , '+',1, 1) > 0 then
result := to_timestamp(LPAD(str, INSTR(str , '+',1, 1)-2), format);
else
result := to_timestamp(str, format);
end if;
exception
when others then
result := null;
end;
--to_timestamp(replace(replace(str, dbtimezone, ''), ' +00:00', ''));
return (result);
end str_timestamp2timestamp;
*/

/*
function str_timestamp2timestamp(str in varchar2)
return date
is
result   date;
begin
begin

select to_date(substr(str,1,3)||decode(substr(str,4,3), 'JAN', '01',
'FEB', '02',
'MAR', '03',
'APR', '04',
'MAY', '05',
'JUN', '06',
'JUL', '07',
'AUG', '08',
'SEP', '09',
'OCT', '10',
'NOV', '11',
'DEC', '12')||substr(str,7,12)||substr(str,26,3), 'DD-MM-YY HH.MI.SS AM')
into result
from dual;

exception
when others then
result := null;
end;
return (result);
end str_timestamp2timestamp;
*/

/*
function str_timestamp2timestamp(str in varchar2)
return date
is
result   date;
begin
begin

select to_date(substr(str,1,3)||decode(substr(str,4,3), 'JAN', '01',
'Ѳ�', '01',
'���', '01',

'FEB', '02',
'���', '02',
'���', '02',

'MAR', '03',
'���', '03',
'���', '03',

'APR', '04',
'�²', '04',
'���', '04',

'MAY', '05',
'���', '05',
'���', '05',

'JUN', '06',
'���', '06',
'���', '06',

'JUL', '07',
'���', '07',
'���', '07',

'AUG', '08',
'���', '08',
'���', '08',

'SEP', '09',
'���', '09',
'���', '09',

'OCT', '10',
'���', '10',
'���', '10',

'NOV', '11',
'���', '11',
'���', '11',

'DEC', '12',
'���', '12',
'���', '12'
)||substr(str,7,12)||substr(str,26,3), 'DD-MM-YY HH.MI.SS AM')
into result
from dual;

exception
when others then
result := null;
end;
return (result);
end str_timestamp2timestamp;
*/

FUNCTION str_timestamp2timestamp(str IN VARCHAR2) RETURN DATE IS
v_tmp_1_3   CHAR(3);
v_tmp_4_6   CHAR(3);
v_tmp_7_18  CHAR(12);
v_tmp_26_28 CHAR(3);
BEGIN
v_tmp_1_3 := substr(str,1,3);
v_tmp_4_6 := substr(str,4,3);
v_tmp_7_18 := substr(str,7,12);
v_tmp_26_28 := substr(str,26,3);

RETURN to_date(v_tmp_1_3||
( CASE
WHEN v_tmp_4_6 IN ('JAN', 'Ѳ�', '���') THEN '01'
WHEN v_tmp_4_6 IN ('FEB', '���', '���') THEN '02'
WHEN v_tmp_4_6 IN ('MAR', '���', '���') THEN '03'
WHEN v_tmp_4_6 IN ('APR', '�²', '���') THEN '04'
WHEN v_tmp_4_6 IN ('MAY', '���', '���') THEN '05'
WHEN v_tmp_4_6 IN ('JUN', '���', '���') THEN '06'
WHEN v_tmp_4_6 IN ('JUL', '���', '���') THEN '07'
WHEN v_tmp_4_6 IN ('AUG', '���', '���') THEN '08'
WHEN v_tmp_4_6 IN ('SEP', '���', '���') THEN '09'
WHEN v_tmp_4_6 IN ('OCT', '���', '���') THEN '10'
WHEN v_tmp_4_6 IN ('NOV', '���', '���') THEN '11'
WHEN v_tmp_4_6 IN ('DEC', '���', '���') THEN '12'
END )
||v_tmp_7_18||v_tmp_26_28, 'DD-MM-YY HH.MI.SS AM');
EXCEPTION
WHEN OTHERS THEN
RETURN NULL;
END str_timestamp2timestamp;

FUNCTION TIMESTAMP2STR(
P_TIMESTAMP IN TIMESTAMP DEFAULT SYSTIMESTAMP
)
RETURN VARCHAR2
IS
BEGIN
RETURN TO_CHAR(P_TIMESTAMP, BCP5_CONSTANTS_PKG.FORMAT_TO_DATETIME);
END;

-- ��������� - �������� �� ���� �������, ��� ������ ������ ����
function get_acc_name(acc_num in varchar2,
acc_ccy in varchar2,
system_id in varchar2)
return varchar2
is
result bcpaycache.acc_name%type;
begin
begin
-- �������� ���� � ���������� ���������� ���������
select nvl(docs.acc_name, '')
into result
from bcpaycache docs
where (
(docs.acc_deb = acc_num and docs.request = 'N')
or
(docs.acc_cr = acc_num and docs.request = 'Y')
)
and docs.currency = acc_ccy
and get_system_id_by_paysys(docs.paysys) = to_number(system_id)
and rownum = 1
order by id desc;

exception
when others then
result := '';
end;
return (result);
end get_acc_name;

-- --------------------------------------------------------------------------
-- �������� ID ��������, �� ���� ��������� ��������
-- --------------------------------------------------------------------------
function get_branch_by_doc(p_doc_id in bcpaycache.id%type)
return bcpaycache.branch%type
is
res bcpaycache.branch%type;
begin
-- ��������� ID ��������
select doc.branch
into res
from bcpaycache doc
where doc.id = p_doc_id;
-- ��������� ID ��������
return res;
exception
-- ���� ��� ��������� ID �������� ������� ������� (������ ��������� �� ���� ����) - ��������� NULL
when others then
return null;
end;

-- --------------------------------------------------------------------------
-- �������� ID ����� ������� ���, �� ���� ��������� ��������
-- --------------------------------------------------------------------------
/*
20150409 old code
function get_group_by_doc(
p_doc_id    in bcpaycache.id%type -- ID ���������
)
return bcp_doc_for_control.group_id%type
is
v_group_id           bcp_doc_for_control.group_id%type; -- ID �����
v_branch_id          bcpaycache.branch%type;
begin

-- �������� ID ��������, �� ���� ��������� ��������
v_branch_id := get_branch_by_doc(p_doc_id);

-- ��������� ����� ������� ��� �� ���������
select br.group_id
into v_group_id
from bcp_branch_to_group br
where br.branch_id = v_branch_id;
-- ��������� ������������� ����� ������� ���
return v_group_id;
exception
-- � ������� ������� - ��������� NULL
when others then
return null;
end;
*/
-- --------------------------------------------------------------------------------------
-- ���������, ���� �� ������ �������� �������������� �������
-- --------------------------------------------------------------------------------------
function is_corporate(
p_doc_id  in bcpaycache.id%type
)
return boolean
is
v_is_corporate   bcpaycache.is_corporate%type;
begin
-- ������ �������� ����
select b.is_corporate
into v_is_corporate
from bcpaycache b
where b.id = p_doc_id;

-- ���������
if (v_is_corporate = 'Y') then
-- �� �������������� �������
return true;
end if;

-- �� ���������������� �������
return false;
end;

-- --------------------------------------------------------------------------------------
-- ���������, ���� �� ������ �������� ��������� ����������
-- --------------------------------------------------------------------------------------
function is_request(
p_doc_id  in bcpaycache.id%type
)
return boolean
is
v_is_request   bcpaycache.is_corporate%type;
begin
-- ������ �������� ����
select b.request
into v_is_request
from bcpaycache b
where b.id = p_doc_id;

-- ���������
if (v_is_request = 'Y') then
-- ��������� ����������
return true;
end if;

-- �� ��������� ����������
return false;
end;

-- --------------------------------------------------------------------------------------
-- ������� - ���������� ���� ATTRIBUTES �������� ���� ������������� � ������� YYYYMMDD
-- --------------------------------------------------------------------------------------
function get_valuedate_YYYYMMDD_by_att(
p_attributes  in bcpaycache.attributes%type
)
return varchar2
is
v_instr       number;
stmt          varchar2(50);
begin

-- �������� �������, � �������� ���������� �������������� ���� �������������
stmt := bcp3_settings_pkg.G_STMT_FOR_VALUEDATE_BY_ATTR;

-- ���� ��������� ���� ������������� �� �����
v_instr := REGEXP_INSTR(p_attributes, stmt);

-- ���� �������������� ���� ������������� �� �������
--if (v_instr = 0) then
if ( (v_instr = 0) or (v_instr < 0) or (v_instr is null) ) then
--       return 'YYYYMMDD';
return null;
else
-- ���� �������������� ���� ������������� �������
return '20' || SUBSTR (p_attributes, v_instr + 2, 6);
end if;

return '        ';
end;

-- --------------------------------------------------------------------------------------
-- ������� - ���������� ���� ATTRIBUTES �������� ���� ������������� � ������� YYYYMMDD
-- --------------------------------------------------------------------------------------
function get_valuedate_YYYY_MM_DD_by_at(
p_attributes  in bcpaycache.attributes%type
)
return varchar2
is
v_instr       number;
stmt          varchar2(50);
begin

-- �������� �������, � �������� ���������� �������������� ���� �������������
stmt := bcp3_settings_pkg.G_STMT_FOR_VALUEDATE_BY_ATTR;

-- ���� ��������� ���� ������������� �� �����
v_instr := REGEXP_INSTR(p_attributes, stmt);

-- ���� �������������� ���� ������������� �� �������
--if (v_instr = 0) then
if ( (v_instr = 0) or (v_instr < 0) or (v_instr is null) ) then
return null;
else
-- ���� �������������� ���� ������������� �������
return '20' || SUBSTR (p_attributes, v_instr + 2, 2) || '-' ||
SUBSTR (p_attributes, v_instr + 4, 2) || '-' ||
SUBSTR (p_attributes, v_instr + 6, 2);
end if;

return NULL;
end;

-- --------------------------------------------------------------------------------------
-- ������� - ���������� ���� ATTRIBUTES �������� ����� � ����� �������� ������� �� ��
-- --------------------------------------------------------------------------------------
function get_passport_dt_by_attributes(
p_attributes  in bcpaycache.attributes%type
)
return varchar2
is
v_instr       number;
stmt          varchar2(50);
begin

-- �������� �������, � �������� ���������� �������������� ����� � ����� ��������
stmt := bcp3_settings_pkg.G_STMT_FOR_PASSPORT_DT_BY_ATTR;

-- ���� ��������� ���� ������������� �� �����
v_instr := REGEXP_INSTR(p_attributes, stmt);

-- ���� �������������� ����� � ����� �������� �� �������
if (v_instr = 0) then
return '        ';
else
-- ���� �������������� ����� � ����� �������� �������
return SUBSTR (p_attributes, v_instr + 2, 8);
end if;

return '        ';
end;

-- --------------------------------------------------------------------------------------
-- ������� - ���������� ���� ATTRIBUTES �������� ����� � ����� �������� ������� �� ��
-- --------------------------------------------------------------------------------------
function get_passport_kt_by_attributes(
p_attributes  in bcpaycache.attributes%type
)
return varchar2
is
v_instr       number;
stmt          varchar2(50);
begin

-- �������� �������, � �������� ���������� �������������� ����� � ����� ��������
stmt := bcp3_settings_pkg.G_STMT_FOR_PASSPORT_KT_BY_ATTR;

-- ���� ��������� ���� ������������� �� �����
v_instr := REGEXP_INSTR(p_attributes, stmt);

-- ���� �������������� ����� � ����� �������� �� �������
if (v_instr = 0) then
return '        ';
else
-- ���� �������������� ����� � ����� �������� �������
return SUBSTR (p_attributes, v_instr + 2, 8);
end if;

return '        ';
end;

-- --------------------------------------------------------------------------
-- �������� ID ���, �� ���� ��������� ��������
-- --------------------------------------------------------------------------
/*
20150409 old code
function get_cbo_by_doc (
pin_doc_id    in bcpaycache.id%type -- ID ���������
)
return varchar2
is
vn_group_id   number;
vn_cbo_id     varchar2(4);
begin

-- �������� ID �����, �� ���� ��������� ��������
vn_group_id := get_group_by_doc(pin_doc_id);

-- ��������� ��� �� ������
select gr.cbo_id
into vn_cbo_id
from bcp_group_to_cbo gr
where gr.id = vn_group_id;

-- ��������� ������������� ���
return vn_cbo_id;

exception
-- � ������� ������� - ��������� NULL
when others then
return null;
end;
*/

-- --------------------------------------------------------------------------
-- �������, ��� ���������� ID ������, �� �������� � ����������� �� ��������� ������� �������� ��������� ������
-- 0 = ����������� ����� - ������������������ ������
-- 1 = ����� ������ ������ ������ � ��������� ����������� �������
-- --------------------------------------------------------------------------
/*
function get_mode_proc_payms_of_cp_accs
return varchar2
is
vd_time_beg_auto_of_corp_accs       date;
vd_time_end_auto_of_corp_accs       date;
vd_time_now                         date;
begin

-- ������ �������� ������ ������� �������������� ������ �� ������������� ��������
vd_time_beg_auto_of_corp_accs := to_date(bcp3_settings_pkg.G_TIME_BEG_AUTO_OF_CORP_ACC, 'HH24MISS');
-- ������ �������� ����� ������� �������������� ������ �� ������������� ��������
vd_time_end_auto_of_corp_accs := to_date(bcp3_settings_pkg.G_TIME_END_AUTO_OF_CORP_ACC, 'HH24MISS');
-- ���������� ���������� �����
vd_time_now := to_date(to_char(sysdate, 'HH24MISS'), 'HH24MISS');

-- ����������� ����������:
if (vd_time_now between vd_time_beg_auto_of_corp_accs and vd_time_end_auto_of_corp_accs) then
-- � ������, ���� �� �������� � �������� => �������� ������ �������������� ��������� �� ������������� ��������
return '0';
else
-- � ������, ���� �� �� �������� � �������� => �������� ������ ������ (������ ������ � �������� �������������� ���)
return '1';
end if;

-- �� ����������� ����� ����� ������� ������
return '1';
end;
*/
-- ---------------------------------------------------------------------------
-- �������� ���� ��������
-- ---------------------------------------------------------------------------
function get_date_now
return varchar2
is
begin
return to_char(sysdate, 'YYYYMMDD');
end;

-- ---------------------------------------------------------------------------
-- �������� ��� ���������
-- ---------------------------------------------------------------------------
function get_time_now
return varchar2
is
begin
return to_char(sysdate, 'HH24MISS');
end;

-- ---------------------------------------------------------------------------
-- ���������, �� � ��������� ��� ����������� ����� ��� ������� ��������� ������������� �볺���
-- ---------------------------------------------------------------------------
function is_oper_time_for_proc_corp_doc
return varchar2
is
vd_time_now date;
vd_time_operational_end date;
vv_stub varchar2(8);
begin

vv_stub := to_char(trunc(sysdate), 'YYYYMMDD');

-- ������ �������� ����������� ����
vd_time_now := to_date(vv_stub||get_time_now, 'YYYYMMDD'||'HH24MISS');

-- ����������� ��� ���������
bcp3_settings_pkg.init_settings;

-- � ������� ��������� ������ ���, ���� ���������� ���������� ���
vd_time_operational_end := to_date(vv_stub||bcp3_settings_pkg.G_TIME_OPERATIONAL_END, 'YYYYMMDD'||'HH24:MI:SS');

-- �������� ���:
-- ���� ��������� ��� ������ �����, ��� ��������� ���
if (vd_time_now < vd_time_operational_end) then
return 'Y';
else
return 'N';
end if;

return 'N';
end;

-- ---------------------------------------------------------------------------
-- ���������, ������ �� �� ��������� ���� ��������� �������������� ��
-- ---------------------------------------------------------------------------
function is_access_processing_doc_corp(
pin_doc_id  in number
)
return boolean
is
vd_date_now date;
vn_spanPeriod number;
vd_date_doc_crete date;
begin

-- ����� ��������� ��������:
-- ��� ������ �� �������������� ������� �� ����������� ����� �� 30 ���� �� ���� �������� ��
-- ������ ������ ���� � �� ���������� ����������
-- ���������� �� �������� �������
return true;

-- �������� ������� ����
vd_date_now := to_date(bcp3_tools.get_date_now, 'YYYYMMDD');

-- ������ �������� ��������� ���������� ������� ��� ������
vn_spanPeriod := bcp3_settings_pkg.G_COUNT_DAYS_FOR_PR_OF_C_ACC;

-- ������ ���� �������� ��
select to_date(to_char(doc.srcdate), 'YYYYMMDD')
into vd_date_doc_crete
from bcpaycache doc
where doc.id = pin_doc_id;

-- ������� �������
if (vd_date_now > vd_date_doc_crete + vn_spanPeriod) then
-- �� ������ ����� - ������ ����������
return false;
else
-- �� ������ �� ����� - ������ ��������
return true;
end if;

end;

-- ---------------------------------------------------------------------------
-- ���������, ����� �� �� �� ������� �������� ���� ������ (�� ���� DATETIME_ENDPROCESS)
-- ---------------------------------------------------------------------------
function is_end_process_doc_corp(
pin_doc_id  in number
)
return boolean
is
vd_date_now date;
vd_date_doc_end_process date;
begin

-- �������� ������� ����
vd_date_now := to_date(bcp3_tools.get_date_now, 'YYYYMMDD');

-- ������ ���� ��������� ��������� �������������� ��
select trunc(doc.datetime_endprocess)
into vd_date_doc_end_process
from bcpaycache doc
where doc.id = pin_doc_id;

-- ������� �������
if (vd_date_now > vd_date_doc_end_process) then
-- �� ������ �����
return true;
else
-- �� ������ �� �����
return false;
end if;

end;

-- ---------------------------------------------------------------------------
-- ���������, ������ �� �� ��������� ���� ��������� �������������� ��  ������ 2
-- ---------------------------------------------------------------------------
function is_access_proc_doc_corp_ext(
pin_doc_id  in number
)
return varchar2
is
vb_result   boolean;
begin

--
vb_result := is_access_processing_doc_corp(pin_doc_id);

-- ������� �������
if (vb_result) then
-- �� ������ �� ����� - ������ ��������
return 'Y';
else
-- �� ������ ����� - ������ �� ��������
return 'N';
end if;

end;

-- ---------------------------------------------------------------------------
-- ���������, �������� �� ���������� �� ���������� �������� ����� �����
-- ---------------------------------------------------------------------------
function is_recepient_internal(
pin_doc_id  in number
)
return boolean
is
vv_is_recepient_internal varchar(1);
begin

-- ������ ������ �� ���������
select doc.is_receipter_internal
into vv_is_recepient_internal
from bcpaycache doc
where doc.id = pin_doc_id;

-- ����������� ���������� ���������
if (vv_is_recepient_internal = 'Y') then
return true;
else
return false;
end if;

end;

-- --------------------------------------------------------------------------------------
-- ���������, ���� �� ������ �������� � ������� "Z" ("�������� �������������� ������")
-- --------------------------------------------------------------------------------------
/*
function is_delay_doc_corp(
p_doc_id  in bcpaycache.id%type
)
return boolean
is
v_status varchar2(1);
begin
-- ������ �������� ����
select b.status
into v_status
from bcpaycache b
where b.id = p_doc_id;

-- ���������
if (v_status = BCP5_CONSTANTS_PKG.DOC_STATUS_Z) then
-- �� ��������� � ������� "Z" ("�������� �������������� ������")
return true;
end if;

-- Otherwise
return false;
end;
*/
-- --------------------------------------------------------------------------------------
-- ���������, �� ����������� ��������, �� ����������� � ������ 'Z'
-- --------------------------------------------------------------------------------------
function is_locked_delay_doc(
pin_doc_id                  in  bcpaycache.id%type,
pov_supervisor_id           out varchar2,
pov_supervisor_lock_date    out date,
pov_supervisor_name         out varchar2
)
return boolean
is
begin

-- �� ������ ������ �������� - � ������� Z �� ��������
--if (not is_delay_doc_corp(pin_doc_id)) then
if (not bcp3_document_pkg.get_doc_status(pin_doc_id) = BCP5_CONSTANTS_PKG.DOC_STATUS_Z ) then
-- ���� ��� - ������ ���������� ������ ��������
return true;
end if;

select trim(doc.lock_supervisor_id),
doc.lock_date,
trim(doc.lock_supervisor_name)
into pov_supervisor_id,
pov_supervisor_lock_date,
pov_supervisor_name
from BCP_DOC_FOR_DELAY doc
where doc.document_id = pin_doc_id;

-- ���������
if (pov_supervisor_id IS NULL) then
-- �� �� �����������
return false;
end if;

-- Otherwise
-- �� �����������
return true;
end;

-- --------------------------------------------------------------------------------------
-- ������� ��������� ������������� ���� � ������� YYYYMMDD
-- --------------------------------------------------------------------------------------
FUNCTION DATE_TO_CHAR_YYYYMMDD(P_DATE IN DATE DEFAULT SYSDATE)
RETURN VARCHAR2
IS
BEGIN
-- ��� ������� "��� ���������" - ��������� ������ �������� ����
IF (P_DATE IS NULL) THEN RETURN TO_CHAR(SYSDATE, BCP5_CONSTANTS_PKG.GV_FORMAT_DATE_YYYYMMDD); END IF;
-- ��������� �������
RETURN TO_CHAR(P_DATE, BCP5_CONSTANTS_PKG.GV_FORMAT_DATE_YYYYMMDD);
END;

-- --------------------------------------------------------------------------------------
-- ������� ��������� ������������� ���� � ������� YYYY-MM-DD
-- --------------------------------------------------------------------------------------
FUNCTION DATE_TO_CHAR_YYYY_MM_DD(P_DATE IN DATE)
RETURN VARCHAR2
IS
BEGIN
RETURN TO_CHAR(P_DATE, BCP5_CONSTANTS_PKG.GV_FORMAT_DATE_YYYY_MM_DD);
END;

-- --------------------------------------------------------------------------------------
-- ������� ��������� ������������� ���� � ������� HH24:MI:SS
-- --------------------------------------------------------------------------------------
FUNCTION TIME_TO_CHAR_HH24_MI_SS(P_DATE IN DATE)
RETURN VARCHAR2
IS
BEGIN
RETURN TO_CHAR(P_DATE, BCP5_CONSTANTS_PKG.GV_FORMAT_DATE_HH24_MI_SS);
END;

-- --------------------------------------------------------------------------------------
-- ������� ��������� ������������� ���� � ������� HH24MISS
-- --------------------------------------------------------------------------------------
FUNCTION TIME_TO_CHAR_HH24MISS(P_DATE IN DATE)
RETURN VARCHAR2
IS
BEGIN
RETURN TO_CHAR(P_DATE, BCP5_CONSTANTS_PKG.GV_FORMAT_DATE_HH24MISS);
END;

-- --------------------------------------------------------------------------------------
-- ������� ��������� ������������� ���� � ������� HH24MISS
-- --------------------------------------------------------------------------------------
FUNCTION DATE_TO_CHAR_YYYYMMDDHH24MISS(P_DATE IN DATE)
RETURN VARCHAR2
IS
BEGIN
RETURN TO_CHAR(P_DATE, BCP5_CONSTANTS_PKG.GV_FORM_DATE_YYYYMMDDHH24MISS);
END;

-- --------------------------------------------------------------------------------------
-- ������� ���������� �������
-- --------------------------------------------------------------------------------------
FUNCTION CLEAN_NOTPRINTED_SYMBOLS( p_str IN VARCHAR2 ) RETURN VARCHAR2 IS
v_res VARCHAR2(200);
BEGIN

SELECT
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(
REPLACE(p_str,
CHR(31),'_'),
CHR(30),'_'),
CHR(29),'_'),
CHR(28),'_'),
CHR(27),'_'),
CHR(26),'_'),
CHR(25),'_'),
CHR(24),'_'),
CHR(23),'_'),
CHR(22),'_'),
CHR(21),'_'),
CHR(20),'_'),
CHR(19),'_'),
CHR(18),'_'),
CHR(17),'_'),
CHR(16),'_'),
CHR(15),'_'),
CHR(14),'_'),
CHR(13),'_'),
CHR(12),'_'),
CHR(11),'_'),
CHR(10),'_'),
CHR(9),'_'),
CHR(8),'_'),
CHR(7),'_'),
CHR(6),'_'),
CHR(5),'_'),
CHR(4),'_'),
CHR(3),'_'),
CHR(2),'_'),
CHR(1),'_'),
CHR(0),'_')
INTO v_res
FROM dual;
RETURN v_res;
END;

begin
-- ������������� ��������
bcp3_settings_pkg.init_settings;
-- �������������������� �������������� ������� �� ������� �����������
init_log_level;
end bcp3_tools;
/

