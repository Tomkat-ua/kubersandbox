create or replace package bcp3_tools
/******************************************************************************
   $Date:15.02.2021 12:55:50$
        $Author:V.Kostevych$
   $Id:bcp3_tools.pks$
                $Revision:3$
  $HeadURL:https://nexus.test.kv.aval/repository/BCPay-Deliveries/BCPay-DB/BCPay-DB-vx.x.x_20210215.zip$
     $JIRA:BR-1$
******************************************************************************/
--5;
as

   /* *****************************************************************************
      NAME:       bcp3_tools
      PURPOSE:

      REVISIONS:
      Ver        Date        Author           Jira           Description
      ---------  ----------  ---------------  -------------  ------------------------------------
      5.1.7      20150910    OFrolyak                        1. ������� ��������� � ��������� str_timestamp2timestamp
      5.1.13     20151208    OFrolyak                        2. ������� ��������� � ��������� CLEAN_NOTPRINTED_SYMBOLS
      5.2.4      20160912    OFrolyak         SBCPAY-797     3. �������������� ��������� is_delay_doc_corp
      5.2.5      20161010    OFrolyak                        4. ������� ��������� � ��������� get_meth_disp_acc_balance, init_log_level (added hint RESULT_CACHE)

      ***************************************************************************** */

   procedure to_log(        p_description  in varchar2,
                            p_message_type in VARCHAR2,
                            p_session_id   IN NUMBER );

  -- ������� ������ ������� �������� ��� �������: �� - 1, �+ - 0
  function get_system_id_by_paysys( str in varchar2
    )
    return number;

  -- -------------------------------------------------------------------------------------------
  -- �������� ���� ���������, �� �������� ������, �� ��������� �������
  -- (��� ������������ ���� ���� ��� ���������� ������� �� �������)
  -- � ����������� ���, ���� ��������� �������.
  -- -------------------------------------------------------------------------------------------
  function get_pending_docs_amt_with_role( p_account         in varchar2, -- ����� �������
                                           p_account_ccy     in varchar2, -- ������ �������
                                           p_role            in varchar2, -- ����
                                           p_bcp_system_char in varchar2 --
    )
    return number; -- ���� ��������� "� �������" �� ����� ������� (� �������)

  function str_timestamp2timestamp( str in varchar2
    )
    return date;

  FUNCTION TIMESTAMP2STR(
    P_TIMESTAMP IN TIMESTAMP DEFAULT SYSTIMESTAMP
    )
  RETURN VARCHAR2;

  function get_acc_name( acc_num in varchar2,
                         acc_ccy in varchar2,
                         system_id in varchar2
    )
    return varchar2;

  function get_branch_by_doc( p_doc_id in bcpaycache.id%type
    )
    return bcpaycache.branch%type;

   function is_corporate(
     p_doc_id  in bcpaycache.id%type
   )
   return boolean;

   function get_valuedate_YYYYMMDD_by_att(
     p_attributes  in bcpaycache.attributes%type
   )
   return varchar2;

   function get_valuedate_YYYY_MM_DD_by_at(
     p_attributes  in bcpaycache.attributes%type
   )
   return varchar2;

   function get_passport_dt_by_attributes(
     p_attributes  in bcpaycache.attributes%type
   )
   return varchar2;

   function get_passport_kt_by_attributes(
     p_attributes  in bcpaycache.attributes%type
   )
   return varchar2;

   function is_request(
     p_doc_id  in bcpaycache.id%type
   )
   return boolean;

   function get_date_now
   return varchar2;

   function get_time_now
   return varchar2;

   function is_oper_time_for_proc_corp_doc
   return varchar2;

   function is_access_processing_doc_corp(
     pin_doc_id  in number
     )
   return boolean;

   function is_end_process_doc_corp(
     pin_doc_id  in number
     )
   return boolean;

   function is_recepient_internal(
     pin_doc_id  in number
     )
   return boolean;

   function is_access_proc_doc_corp_ext(
     pin_doc_id  in number
     )
   return varchar2;

   function is_locked_delay_doc(
     pin_doc_id                  in  bcpaycache.id%type,
     pov_supervisor_id           out varchar2,
     pov_supervisor_lock_date    out date,
     pov_supervisor_name         out varchar2
   )
   return boolean;

  -- ������� ��������� ������������� ���� � ������� YYYYMMDD
  FUNCTION DATE_TO_CHAR_YYYYMMDD(P_DATE IN DATE DEFAULT SYSDATE) RETURN VARCHAR2;
  -- ������� ��������� ������������� ���� � ������� YYYY-MM-DD
  FUNCTION DATE_TO_CHAR_YYYY_MM_DD(P_DATE IN DATE) RETURN VARCHAR2;
  -- ������� ��������� ������������� ���� � ������� HH24:MI:SS
  FUNCTION TIME_TO_CHAR_HH24_MI_SS(P_DATE IN DATE) RETURN VARCHAR2;
  -- ������� ��������� ������������� ���� � ������� HH24MISS
  FUNCTION TIME_TO_CHAR_HH24MISS(P_DATE IN DATE) RETURN VARCHAR2;
  -- ������� ��������� ������������� ���� � ������� YYYYMMDDHH24MISS
  FUNCTION DATE_TO_CHAR_YYYYMMDDHH24MISS(P_DATE IN DATE) RETURN VARCHAR2;
   -- ������� ���������� �������
   FUNCTION CLEAN_NOTPRINTED_SYMBOLS( p_str IN VARCHAR2 ) RETURN VARCHAR2;

end bcp3_tools;
/

